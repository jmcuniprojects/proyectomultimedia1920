﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Windows.Media.Audio;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// La plantilla de elemento Página en blanco está documentada en https://go.microsoft.com/fwlink/?LinkId=234238

namespace AmbientUWP
{
    /// <summary>
    /// Una página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    internal sealed partial class Channel : Page
    {
        //Atributos necesarios
        public string titleName;
        private string previousText;
        private Home parent;
        private ExplorerItem selectedItem;
        private IMessenger messenger;
        private List<ExplorerItem> SearchResult;
        public AudioFileInputNode _fileInputNode { get; private set; }
        public Volume volume;
        private bool cyclic;
        private int minTime;
        private int maxTime;

        public Channel()
        {
            titleName = "Ejemplo";
            InitializeComponent();
            messenger = Messenger.Default;
            messenger.Register<KeyValuePair<string, double>>(this, VolAction);
            cyclic = false;
            minTime = (int)MinSlider.Value;
            maxTime = (int)MaxSlider.Value;
            volume = new Volume(100);
            DataContext = this;
        }

        public Channel(ref Home parent)
        {
            titleName = "Ejemplo";
            messenger = Messenger.Default;
            messenger.Register<KeyValuePair<string, double>>(this, VolAction);
            this.parent = parent;
            volume = new Volume(100);
            InitializeComponent();
            arbol.ItemsSource = AmbientUWP.Data.LocalVar.tree;
        }

        //Activa los controles cíclicos
        private void CycleToggle_Toggled(object sender, RoutedEventArgs e)
        {
            MinBox.IsEnabled = !MinBox.IsEnabled;
            MaxBox.IsEnabled = !MaxBox.IsEnabled;
            MinSlider.IsEnabled = !MinSlider.IsEnabled;
            MaxSlider.IsEnabled = !MaxSlider.IsEnabled;
            cyclic = !cyclic;
            Task.Run(() => randomLoop());
        }

        //Función que correrá en el hilo cíclico
        private void randomLoop()
        {
            Random rand = new Random();
            while (cyclic)
            {
                if (_fileInputNode.Duration.TotalSeconds - _fileInputNode.Position.TotalSeconds < 0.01)
                {
                    System.Threading.Thread.Sleep(rand.Next(minTime, maxTime) * 100);
                    if (cyclic)
                        _fileInputNode.Reset();
                }
                else
                {
                    if (((int)_fileInputNode.Duration.TotalSeconds / 5) * 100 < 500)
                        System.Threading.Thread.Sleep(500);
                    else
                        System.Threading.Thread.Sleep(((int)_fileInputNode.Duration.TotalSeconds / 5) * 100);
                }
            }
        }

        //Actualiza el volumen al recibir un mensaje de la página principal
        private void VolAction(KeyValuePair<string, double> args)
        {
            if (args.Key == titleName)
            {
                volume.Vol = args.Value;
                Volslider.Value = volume.Vol;
                if (_fileInputNode != null)
                {
                    _fileInputNode.OutgoingGain = volume.Vol / 100;
                }
            }
        }

        //Actualiza el tiempo mínimo del ciclo
        private void MinSlider_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (MinSlider.Value <= MaxSlider.Value)
            {
                minTime = (int)MinSlider.Value;
            }
        }

        //Actualiza el tiempo máximo del ciclo
        private void MaxSlider_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (MinSlider.Value <= MaxSlider.Value)
            {
                maxTime = (int)MaxSlider.Value;
            }
        }

        //Actualiza el nombre del canal
        private void NameBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            titleName = NameBox.Text;
            if (parent != null)
                parent.RefreshUI();
        }

        //Guarda el estado actual
        private void NameBox_GettingFocus(UIElement sender, Windows.UI.Xaml.Input.GettingFocusEventArgs args)
        {
            previousText = NameBox.Text;
        }

        //Restaura el nombre anterior si el nombre está en blanco
        private void NameBox_LosingFocus(UIElement sender, Windows.UI.Xaml.Input.LosingFocusEventArgs args)
        {
            if (NameBox.Text.Length == 0)
            {
                NameBox.Text = previousText;
            }
        }

        //Realiza la búsqueda en la lista indexada
        private void AutoSuggestBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                SearchResult = Data.LocalVar.doSearch(sender.Text.ToLower());


                if (SearchResult.Count < 0)
                    SearchResult.Clear();
                sender.ItemsSource = SearchResult;
            }
        }

        //Muestra los resultados de la búsqueda
        private void AutoSuggestBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if (args.ChosenSuggestion != null && args.ChosenSuggestion is ExplorerItem item)
            {
                sender.Text = item.DisplayName;
                sender.ItemsSource = null;
                selectedItem = item;
            }
        }

        /// <summary>
        /// Take the file specified on parameters an create an AudioFileInputNode with it
        /// </summary>
        private async Task CreateFileInputNode(string path)
        {
            StorageFile file = await StorageFile.GetFileFromPathAsync(path);
            CreateAudioFileInputNodeResult result = await parent.mainTab._graph.CreateFileInputNodeAsync(file);

            if (result.Status != AudioFileNodeCreationStatus.Success)
            {
                throw new Exception(result.Status.ToString());
            }

            _fileInputNode = result.FileInputNode;
            volume.Vol = Volslider.Value;
            _fileInputNode.OutgoingGain = volume.Vol / 100;
        }

        private void Volslider_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            volume.Vol = e.NewValue;
            if (_fileInputNode != null)
            {
                _fileInputNode.OutgoingGain = volume.Vol / 100;
            }
        }

        //Carga el sonido seleccionado en el reproductor
        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectedItem != null)
            {
                SelectedBox.Text = selectedItem.DisplayName;
                CreateFileInputNode(selectedItem.Path);
                parent.unConect();
            }
        }

        //Selecciona el elemento del árbol
        private void arbol_ItemInvoked(TreeView sender, TreeViewItemInvokedEventArgs args)
        {
            if (args.InvokedItem is ExplorerItem item && item.Type == ExplorerItem.ExplorerItemType.File)
            {
                args.Handled = true;
                selectedItem = item;
            }
        }

        //Actualiza el volumen
        private void Page_GettingFocus(UIElement sender, Windows.UI.Xaml.Input.GettingFocusEventArgs args)
        {
            Volslider.Value = volume.Vol;
        }

        internal Volume Volume
        {
            get => default;
            set
            {
            }
        }

        public ExplorerItem ExplorerItem
        {
            get => default;
            set
            {
            }
        }
    }

    /// <summary>
    /// Clase auxiliar que implementa el protocolo de notificación, cuando se actualiza una propiedad notificará a los elementos de la interfaz
    /// </summary>
    internal class Volume : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        private double vol;
        public double Vol
        {
            get { return vol; }
            set { SetField(ref vol, value, "Vol"); }
        }

        public Volume(double Ivol)
        {
            Vol = Ivol;
        }
    }
}
