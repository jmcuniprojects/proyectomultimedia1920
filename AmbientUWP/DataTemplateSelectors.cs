﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace AmbientUWP
{
    /// <summary>
    /// En este fichero almacenamos los selectores de plantillas para que el código XAML utilize una plantilla en función del objeto que vaya a representar.
    /// </summary>
    public class ExplorerItemTemplateSelector : DataTemplateSelector
    {
        public DataTemplate FolderTemplate { get; set; }
        public DataTemplate FileTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item)
        {
            var explorerItem = (ExplorerItem)item;
            if (explorerItem.Type == ExplorerItem.ExplorerItemType.Folder) return FolderTemplate;

            return FileTemplate;
        }
    }

    internal class NavigationViewItemSelector : DataTemplateSelector
    {
        public DataTemplate GeneralTemplate { get; set; }
        public DataTemplate ChannelTemplate { get; set; }
        public DataTemplate SeparatorTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item)
        {
            if (item.GetType() == typeof(NavigationViewItemSeparator))
                return SeparatorTemplate;
            else if (item.GetType() == typeof(GeneralPage))
                return GeneralTemplate;
            else
                return ChannelTemplate;
        }
    }
}
