﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using Windows.Storage;

namespace AmbientUWP
{
    /// <summary>
    /// Un item explorador es una abstracción de carpetas y ficheros, de forma que el mismo objeto nos valga para los dos
    /// </summary>
    public class ExplorerItem
    {
        //Atributos
        public enum ExplorerItemType
        {
            Folder = 0,
            File = 1
        }
        //Los miembros StorageFile y StorageFolder son los propietarios de la API de Windows, asi que además almacenan permisos
        public StorageFile File;
        public StorageFolder Folder;
        public string DisplayName;
        public string Path;
        public ExplorerItemType Type;
        public List<ExplorerItem> Children;

        //Constructores
        public ExplorerItem(StorageFile path)
        {
            File = path;
            Path = File.Path;
            Type = ExplorerItemType.File;
            DisplayName = File.DisplayName;
            Children = null;
            Path = File.Path;
        }
        public ExplorerItem(StorageFolder path)
        {
            Folder = path;
            Path = Folder.Path;
            Type = ExplorerItemType.Folder;
            DisplayName = Folder.DisplayName;
            Children = new List<ExplorerItem>();
            getAllElementsAsync();
            Path = Folder.Path;
            cleanEmpty();
        }
        //Este constructor se utiliza para reconstruir un objeto serializado en JSON
        [JsonConstructor]
        public ExplorerItem(StorageFolder folder, StorageFile File, string display, string path, ExplorerItemType type, List<ExplorerItem> child)
        {
            this.File = File;
            Folder = folder;
            DisplayName = display;
            Path = path;
            Type = type;
            Children = child;
        }

        public ExplorerItem(string path)
        {
            Path = path;
            if (Directory.Exists(path))
            {
                Type = ExplorerItemType.Folder;
                DisplayName = path.Substring(path.LastIndexOf("\\") + 1, path.Length - (path.LastIndexOf("\\") + 1));
                Children = new List<ExplorerItem>();
                findFiles();
                cleanEmpty();
            }
            else
            {
                Type = ExplorerItemType.File;
                DisplayName = path.Substring(path.LastIndexOf("\\") + 1, path.LastIndexOf(".") - (path.LastIndexOf("\\") + 1));
                Children = null;
            }
        }

        //en esta función recorremos todos los ficheros de una carpeta para añadirlos a la lista de hijos y filtrar sólo los formatos que conoce AudioGraph
        private void findFiles()
        {
            getAllFiles(Path);
        }

        private void getAllFiles(string father)
        {
            string[] files = Directory.GetFiles(father);
            string[] folders = Directory.GetDirectories(father);
            foreach (string folder in folders)
            {
                Children.Add(new ExplorerItem(folder));
            }
            foreach (string file in files)
            {
                ExplorerItem Item = new ExplorerItem(file);
                if (Item.Path.ToLower().Contains(".mp3") || Item.Path.ToLower().Contains(".acc") || Item.Path.ToLower().Contains(".flac") || Item.Path.ToLower().Contains(".alac") || Item.Path.ToLower().Contains(".wav") || Item.Path.ToLower().Contains(".wma"))
                    Children.Add(Item);
            }
        }

        //Este método es similar al anterior pero ejecutable asíncronamente.
        private async void getAllElementsAsync()
        {
            var folders = await Folder.GetFoldersAsync();
            var files = await Folder.GetFilesAsync();
            foreach (var folder in folders)
            {
                Children.Add(new ExplorerItem(folder));
            }
            foreach (var file in files)
            {
                if (file.FileType.ToLower() == ".mp3" || file.FileType.ToLower() == ".aac" || file.FileType.ToLower() == ".flac" || file.FileType.ToLower() == ".alac" || file.FileType.ToLower() == ".wav" || file.FileType.ToLower() == ".wma")
                    Children.Add(new ExplorerItem(file));
            }
        }

        //Limpia las carpetas vacías de los hijos
        private void cleanEmpty()
        {
            foreach (ExplorerItem child in Children)
            {
                if (child.Type == ExplorerItemType.Folder && child.Children.Count < 1)
                    Children.Remove(child);
            }
        }
    }
}