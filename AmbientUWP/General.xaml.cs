﻿using AmbientUWP.Services;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.Media.Audio;
using Windows.Media.MediaProperties;
using Windows.Media.Playback;
using Windows.Media.Render;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

// La plantilla de elemento Página en blanco está documentada en https://go.microsoft.com/fwlink/?LinkId=234238

namespace AmbientUWP
{

    /// <summary>
    /// Una página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public partial class GeneralPage : Page
    {
        public AudioGraph _graph { get; private set; }

        //Equalizer
        private EqualizerEffectDefinition eqEffectDefinition1;
        private EqualizerEffectDefinition eqEffectDefinition2;
        private EqualizerEffectDefinition eqEffectDefinition3;

        private IMessenger messenger;

        private ObservableCollection<object> channels;
        private bool playing;
        public bool conected { get; set; }
        public bool recording { get; private set; }

        private AudioDeviceOutputNode _deviceOutputNode;
        private AudioSubmixNode submixNode; //for mixing different files
        private AudioFileOutputNode _outputFileNode; //archivo de salida en caso de grabar

        private MediaPlayer Player => PlaybackService.Instance.Player;

        //Record File
        private MediaEncodingProfile fileProfile;
        private String nameRecord;
        private String formatRecord;
        private StorageFolder folderRecord;

        private MediaPlaybackList PlaybackList
        {
            get { return Player.Source as MediaPlaybackList; }
            set { Player.Source = value; }
        }

        public string titleName;
        public GeneralPage()
        {
            titleName = "General";
            InitializeComponent();
            messenger = Messenger.Default;
            channels = new ObservableCollection<object>();

            playing = false;
            conected = false;
            recording = false;
            if (PlaybackList == null)
                PlaybackList = new MediaPlaybackList();
        }

        public void updateList(List<object> channels)
        {
            this.channels.Clear();
            foreach (object channel in channels)
            {
                this.channels.Add(channel);
            }
        }

        public async Task CreateGraph()
        {
            // Specify settings for graph, the AudioRenderCategory helps to optimize audio processing
            AudioGraphSettings settings = new AudioGraphSettings(AudioRenderCategory.Media);

            CreateAudioGraphResult result = await AudioGraph.CreateAsync(settings);

            //Si falla tira excepción
            if (result.Status != AudioGraphCreationStatus.Success)
            {
                throw new Exception(result.Status.ToString());
            }

            _graph = result.Graph;

            //Inicializo el ecualizador
            eqEffectDefinition1 = new EqualizerEffectDefinition(_graph);
            eqEffectDefinition2 = new EqualizerEffectDefinition(_graph);
            eqEffectDefinition3 = new EqualizerEffectDefinition(_graph);


            for (int i = 0; i < 10; i++)
            {
                if (i < 4)
                {
                    eqEffectDefinition1.Bands[i].FrequencyCenter = Data.Parameters.defaultBands[i, 0];
                    eqEffectDefinition1.Bands[i].Bandwidth = Data.Parameters.defaultBands[i, 1];
                    eqEffectDefinition1.Bands[i].Gain = 1;
                }
                else if (i < 7)
                {
                    eqEffectDefinition2.Bands[i % 4].FrequencyCenter = Data.Parameters.defaultBands[i, 0];
                    eqEffectDefinition2.Bands[i % 4].Bandwidth = Data.Parameters.defaultBands[i, 1];
                    eqEffectDefinition2.Bands[i % 4].Gain = 1;
                }
                else if (i < 10)
                {
                    eqEffectDefinition3.Bands[i % 4].FrequencyCenter = Data.Parameters.defaultBands[i, 0];
                    eqEffectDefinition3.Bands[i % 4].Bandwidth = Data.Parameters.defaultBands[i, 1];
                    eqEffectDefinition3.Bands[i % 4].Gain = 1;
                }
            }
        }

        //Funcion para colocar valores de ecualizador si estan cambiados
        private void reDefineEQ()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i < 4)
                {
                    eqEffectDefinition1.Bands[i].FrequencyCenter = Data.Parameters.nonDefaultBands[i, 0];
                    eqEffectDefinition1.Bands[i].Bandwidth = Data.Parameters.nonDefaultBands[i, 1];
                }
                else if (i < 7)
                {
                    eqEffectDefinition2.Bands[i % 4].FrequencyCenter = Data.Parameters.nonDefaultBands[i, 0];
                    eqEffectDefinition2.Bands[i % 4].Bandwidth = Data.Parameters.nonDefaultBands[i, 1];
                }
                else if (i < 10)
                {
                    eqEffectDefinition3.Bands[i % 4].FrequencyCenter = Data.Parameters.nonDefaultBands[i, 0];
                    eqEffectDefinition3.Bands[i % 4].Bandwidth = Data.Parameters.nonDefaultBands[i, 1];
                }
            }
        }

        /// <summary>
        /// Create a node to output audio data to the default audio device (e.g. soundcard)
        /// </summary>
        public async Task CreateDefaultDeviceOutputNode()
        {
            CreateAudioDeviceOutputNodeResult result = await _graph.CreateDeviceOutputNodeAsync();

            if (result.Status != AudioDeviceNodeCreationStatus.Success)
            {
                throw new Exception(result.Status.ToString());
            }

            _deviceOutputNode = result.DeviceOutputNode;
        }

        //Crea el nodo de salida para la grabación según los datos especificados
        public async Task CreateFileOutputNode()
        {
            if (_outputFileNode != null)
            {
                _outputFileNode.Dispose();
            }
            StorageFile file;
            if (await folderRecord.GetFileAsync(nameRecord + formatRecord) == null)
            {
                file = await folderRecord.CreateFileAsync(nameRecord + formatRecord);
            }
            else
            {
                file = await folderRecord.GetFileAsync(nameRecord + formatRecord);
                await file.DeleteAsync();
                file = await folderRecord.CreateFileAsync(nameRecord + formatRecord);
            }

            // File can be null if cancel is hit in the file picker
            if (file == null)
            {
                return;
            }
            CreateAudioFileOutputNodeResult fileOutputNodeResult = await _graph.CreateFileOutputNodeAsync(file, fileProfile);

            _outputFileNode = fileOutputNodeResult.FileOutputNode;
        }

        /// <summary>
        /// Connect all the nodes together to form the graph, each channel has one node
        /// </summary>
        private void ConnectNodes(List<object> channels)
        {
            submixNode = _graph.CreateSubmixNode();

            submixNode.EffectDefinitions.Add(eqEffectDefinition1);
            submixNode.EffectDefinitions.Add(eqEffectDefinition2);
            submixNode.EffectDefinitions.Add(eqEffectDefinition3);

            submixNode.AddOutgoingConnection(_deviceOutputNode);

            //Agrega la salida de archivo si vamos a grabar
            if (recording && _outputFileNode != null)
            {
                submixNode.AddOutgoingConnection(_outputFileNode);
            }
            foreach (Channel channel in channels)
            {
                if (channel._fileInputNode != null)
                {
                    channel._fileInputNode.AddOutgoingConnection(submixNode, 0.5);
                }
            }
        }

        public void PlayButtn_Click(object sender, RoutedEventArgs e, List<object> channels)
        {
            if (!playing)
            {
                //Comprueba que no hay cambios en el ecualizador y si lo hay los aplica
                if (Data.Parameters.bandsChanged)
                {
                    reDefineEQ();
                    Data.Parameters.bandsChanged = false;
                }
                //En caso de que aparezcan cambios en los nodos se reconectan
                if (!conected)
                {
                    if (submixNode != null)
                        submixNode.Dispose();
                    ConnectNodes(channels);
                    conected = true;
                }
                _graph.Start();
                playing = true;
            }
            else
            {
                _graph.Stop();
                playing = false;
            }
        }

        public void NextButtn_Click(object sender, RoutedEventArgs e, List<object> channels)
        {
            _graph.Stop();
            playing = false;
            //Reset a los nodos para poner el avance a 0
            foreach (Channel channel in channels)
            {
                channel._fileInputNode.Reset();
            }
        }

        public void RepeatButtn_Checked(object sender, RoutedEventArgs e, List<object> channels)
        {
            foreach (Channel channel in channels)
            {
                if (channel._fileInputNode.Position >= channel._fileInputNode.Duration)
                {
                    channel._fileInputNode.Seek(channel._fileInputNode.StartTime.Value);
                }
                channel._fileInputNode.LoopCount = null; // infinite loop
            }
        }

        public void RepeatButtn_Unchecked(object sender, RoutedEventArgs e, List<object> channels)
        {
            foreach (Channel channel in channels)
                channel._fileInputNode.LoopCount = 0; // end loop
        }

        private void VolSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            //Se altera el voluman de todos los canales, es decir de la mezcla final
            if (submixNode != null)
                submixNode.OutgoingGain = VolSlider.Value / 100;
        }

        public async Task<bool> RecordButtn_Checked(object sender, RoutedEventArgs e, List<object> channels)
        {
            //Para la reproduccion si no lo estaba (No resetea a 0)
            if (playing)
            {
                _graph.Stop();
                playing = false;
            }
            //Comprueba que los parametros de grabación estan definidos
            //Devuelve si el boton de grabar queda marcado o no
            if (folderRecord != null && nameRecord != null && formatRecord != null && fileProfile != null)
            {
                await CreateFileOutputNode();
                recording = true;
                conected = false;
                PlayButtn_Click(sender, e, channels); //Comienza a reproducir la mezcla en su estado actual
                return true;
            }
            else
            {
                return false;
            }
        }

        public void RecordButtn_Unchecked(object sender, RoutedEventArgs e)
        {
            if (recording)
            {
                recording = false;
                conected = false;
            }
        }

        //Abrir una carpeta para guardar el fichero de grabado
        private async void OpenButtn_Click(object sender, RoutedEventArgs e)
        {
            FolderPicker picker = new FolderPicker();
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.MusicLibrary;
            picker.FileTypeFilter.Add("*");
            folderRecord = await picker.PickSingleFolderAsync();

            if (folderRecord != null)
            {
                PathBox.Text = folderRecord.Path;
            }
        }

        //Aplicar el formato correcto al fichero grabado
        private void FormatPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems[0] is string selectedFormat)
            {
                if (selectedFormat == "MP3")
                {
                    fileProfile = MediaEncodingProfile.CreateMp3(AudioEncodingQuality.High);
                    formatRecord = ".mp3";
                }
                if (selectedFormat == "WAV")
                {
                    fileProfile = MediaEncodingProfile.CreateWav(AudioEncodingQuality.High);
                    formatRecord = ".wav";
                }
                if (selectedFormat == "WMA")
                {
                    fileProfile = MediaEncodingProfile.CreateWma(AudioEncodingQuality.High);
                    formatRecord = ".wma";
                }
            }
        }

        //Almacena el nombre para el fichero grabado
        private void NameBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (NameBox.Text.ToString() != "")
            {
                nameRecord = NameBox.Text.ToString();
            }
        }

        //Aplicar los efectos de ecualización
        private void EQBand1_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            eqEffectDefinition1.Bands[0].Gain = Math.Pow(10, EQBand1.Value / 12);
        }

        private void EQBand2_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            eqEffectDefinition1.Bands[1].Gain = Math.Pow(10, EQBand2.Value / 12);
        }

        private void EQBand3_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            eqEffectDefinition1.Bands[2].Gain = Math.Pow(10, EQBand3.Value / 12);
        }

        private void EQBand4_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            eqEffectDefinition1.Bands[3].Gain = Math.Pow(10, EQBand4.Value / 12);
        }

        private void EQBand5_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            eqEffectDefinition2.Bands[0].Gain = Math.Pow(10, EQBand5.Value / 12);
        }

        private void EQBand6_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            eqEffectDefinition2.Bands[1].Gain = Math.Pow(10, EQBand6.Value / 12);
        }

        private void EQBand7_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            eqEffectDefinition2.Bands[2].Gain = Math.Pow(10, EQBand7.Value / 12);
        }

        private void EQBand8_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            eqEffectDefinition2.Bands[3].Gain = Math.Pow(10, EQBand8.Value / 12);
        }

        private void EQBand9_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            eqEffectDefinition3.Bands[0].Gain = Math.Pow(10, EQBand9.Value / 12);
        }

        private void EQBand10_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            eqEffectDefinition3.Bands[1].Gain = Math.Pow(10, EQBand10.Value / 12);
        }


        //Envia un mensaje al canal correspondiente, avisando de que debe cambiar de volumen
        private void VolSliderTemp_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            Slider slider = (Slider)sender;
            messenger.Send<KeyValuePair<string, double>>(new KeyValuePair<string, double>(slider.Header.ToString(), e.NewValue));
        }

        internal PlaybackService PlaybackService
        {
            get => default;
            set
            {
            }
        }
    }
}
