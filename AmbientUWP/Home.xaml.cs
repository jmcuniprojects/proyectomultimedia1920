﻿using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// La plantilla de elemento Página en blanco está documentada en https://go.microsoft.com/fwlink/?LinkId=234238

namespace AmbientUWP
{
    /// <summary>
    /// Página principal del programa, en esta página nos encontramos la página general y los distintos canales
    /// </summary>
    public sealed partial class Home : Page
    {
        public List<object> channels { get; private set; }

        private List<object> NavigationList;
        public GeneralPage mainTab { get; }
        public ExplorerItem root { get; private set; }

        public Home()
        {
            channels = new List<object>();
            mainTab = new GeneralPage();
            NavigationList = MenuItems().ToList();
            InitializeComponent();
            SetSelectedNavigationItem(0);
            contentFrame.Content = mainTab;
            Rmv_btn.IsEnabled = false;
        }
        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await mainTab.CreateGraph();
            await mainTab.CreateDefaultDeviceOutputNode();
        }

        //Esta función arma la lista de navegación añadiendo los elementos estáticos y los canales dinámicos
        private IEnumerable<object> MenuItems()
        {
            yield return mainTab;
            yield return new NavigationViewItemSeparator();
            foreach (var some in channels)
            {
                yield return some;
            }
        }

        //Refresca la interfaz de la página
        public void RefreshUI()
        {
            NavigationList = MenuItems().ToList();
            mainTab.updateList(channels);
            nvSample.MenuItemsSource = NavigationList;
        }

        //Navega a la página correcta
        private void nvSample_ItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            FrameNavigationOptions navOptions = new FrameNavigationOptions();
            navOptions.TransitionInfoOverride = args.RecommendedNavigationTransitionInfo;
            Page page = mainTab;
            if (sender.SelectedItem.GetType() == NavigationList[0].GetType())
            {
                page = mainTab;
            }
            else
            {
                page = (Channel)NavigationList[NavigationList.IndexOf(sender.SelectedItem)];
            }
            contentFrame.Navigate(page.GetType(), navOptions);
            contentFrame.Content = page;

        }

        //Añade un nuevo canal
        private void Add_btn_Click(object sender, RoutedEventArgs e)
        {
            var me = this;
            Channel newChannel = new Channel(ref me);
            newChannel.titleName = "Channel " + (channels.Count + 1);
            channels.Add(newChannel);
            RefreshUI();
            mainTab.conected = false;
            if (channels.Count == 6)
                Add_btn.IsEnabled = false;
            if (channels.Count > 0)
                Rmv_btn.IsEnabled = true;
        }

        //Cambia al elemento seleccionado en la navigation view
        public void SetSelectedNavigationItem(int index)
        {
            nvSample.SelectedItem = NavigationList[index];
        }

        //Elimina el canal seleccionado o el último, vuelve a general
        private void Rmv_btn_Click(object sender, RoutedEventArgs e)
        {
            int selectedItem = NavigationList.IndexOf(nvSample.SelectedItem) - 2;
            if (nvSample.SelectedItem != nvSample.SettingsItem)
            {
                SetSelectedNavigationItem(0);
                contentFrame.Content = mainTab;
            }
            if (selectedItem == 0 && (channels.Count == 1))
            {
                channels.Clear();
            }
            else if (selectedItem > 0)
            {
                channels.RemoveAt(selectedItem);
            }
            else
            {
                if (channels.Count != 0)
                {
                    channels.RemoveAt(channels.Count - 1);
                }
            }
            RefreshUI();
            mainTab.conected = false;
            if (channels.Count < 6)
                Add_btn.IsEnabled = true;
            if (channels.Count == 0)
                Rmv_btn.IsEnabled = false;
        }

        public void unConect()
        {
            mainTab.conected = false;
        }

        private void PlayButtn_Click(object sender, RoutedEventArgs e)
        {
            mainTab.PlayButtn_Click(sender, e, channels);
        }

        private void NextButtn_Click(object sender, RoutedEventArgs e)
        {
            mainTab.NextButtn_Click(sender, e, channels);
        }

        private void RepeatButtn_Checked(object sender, RoutedEventArgs e)
        {
            mainTab.RepeatButtn_Checked(sender, e, channels);
        }

        private void RepeatButtn_Unchecked(object sender, RoutedEventArgs e)
        {
            mainTab.RepeatButtn_Unchecked(sender, e, channels);
        }

        private async void RecordButtn_Checked(object sender, RoutedEventArgs e)
        {
            RecordButtn.IsChecked = await mainTab.RecordButtn_Checked(sender, e, channels);
        }

        private void RecordButtn_Unchecked(object sender, RoutedEventArgs e)
        {
            mainTab.RecordButtn_Unchecked(sender, e);
        }

        public GeneralPage GeneralPage
        {
            get => default;
            set
            {
            }
        }

        public ExplorerItem ExplorerItem
        {
            get => default;
            set
            {
            }
        }

        internal Channel Channel
        {
            get => default;
            set
            {
            }
        }
    }
}
