﻿using Microsoft.Toolkit.Uwp.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.Storage;

namespace AmbientUWP.Data
{

    /// <summary>
    /// Utilizaremos esta clase como "Base de datos" donde almacenaremos las estructuras de datos comunes a todas las páginas
    /// Por cómo funciona UWP cada página es independiente de las demás, por lo tanto es necesario un método para intercambiar información
    /// </summary>
    public static class LocalVar
    {
        //Es una clase estatica por lo que los atributos deben inicializarse en la declaración
        public static ObservableCollection<ExplorerItem> tree = new ObservableCollection<ExplorerItem>();
        public static List<ExplorerItem> indexedTree = new List<ExplorerItem>();
        private static LocalObjectStorageHelper helper = new LocalObjectStorageHelper();

        public static ExplorerItem ExplorerItem
        {
            get => default;
            set
            {
            }
        }

        public static Settings Settings
        {
            get => default;
            set
            {
            }
        }

        //Para poder realizar búsquedas dentro del árbol es necesario indexar todos los ficheros y carpetas en otra EEDD,
        //en nuestro caso es una lista para poder realizar sentencias de búsqueda ràpidamente (Una hash quizá sea más eficiente)
        public static void IndexList()
        {
            indexedTree.Clear();
            foreach (ExplorerItem item in tree)
            {
                IndexList(item);
            }
        }

        private static void IndexList(ExplorerItem item)
        {
            indexedTree.Add(item);
            if (item.Children != null)
            {
                foreach (var child in item.Children)
                    IndexList(child);
            }
        }

        public static List<ExplorerItem> doSearch(string sentence)
        {
            if (sentence.Length > 0)
                return (indexedTree.FindAll(x => x.DisplayName.ToLower().Contains(sentence)));
            else
                return new List<ExplorerItem>();
        }

        //Esta función almacena en un fichero las configuraciones actuales del programa
        public static void saveSettings()
        {
            List<string> array = new List<string>();
            foreach (ExplorerItem explorerItem in tree)
                array.Add(explorerItem.Path);
            string[] saveArray = array.ToArray();
            helper.SaveFileAsync("Folders", saveArray);
        }

        //Esta función recupera el fichero de configuración y lo aplica
        public static async void loadSettings()
        {
            try
            {
                if (await helper.FileExistsAsync("Folders"))
                {
                    var temp = await helper.ReadFileAsync<string[]>("Folders");
                    for (int i = 0; i < temp.Length; i++)
                    {
                        string path = temp[i];
                        StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(path);
                        tree.Add(new ExplorerItem(folder));
                    }
                    IndexList();
                }
            }
            catch (ArgumentNullException) //Este cath está aqui para evitar que el programa se cierre si no existe el fichero
            {
                tree.Clear();
                indexedTree.Clear();
            }
            catch (NullReferenceException) //Este cath está aqui para evitar que el programa se cierre si no existe el fichero
            {
                tree.Clear();
                indexedTree.Clear();
            }
        }
    }


    /// <summary>
    /// Esta clase representa los parametros del ecualizador por defecto
    /// </summary>
    public static class Parameters
    {
        public static bool bandsChanged = false;

        public static double[,] defaultBands = new double[10, 2]
        {
                {31.5, 0.8},
                {63, 0.8},
                {125, 0.8},
                {250, 0.8},
                {500, 0.8},
                {1000, 0.8},
                {2000, 0.8},
                {4000, 0.8},
                {8000, 0.8},
                {16000, 0.8}
        };

        public static double[,] nonDefaultBands = new double[10, 2]
        {
                {31.5, 0.8},
                {63, 0.8},
                {125, 0.8},
                {250, 0.8},
                {500, 0.8},
                {1000, 0.8},
                {2000, 0.8},
                {4000, 0.8},
                {8000, 0.8},
                {16000, 0.8}
        };
    }
}
