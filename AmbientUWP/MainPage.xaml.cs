﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// La plantilla de elemento Página en blanco está documentada en https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0xc0a

namespace AmbientUWP
{
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    /// 
    internal partial class MainPage : Page
    {
        private Home mainPage;
        private Settings settingsPage;

        public MainPage()
        {
            mainPage = new Home();
            settingsPage = new Settings();

            InitializeComponent();
            SetSelectedNavigationItem(0);
            contentFrame.Content = mainPage;

        }

        public Home Home
        {
            get => default;
            set
            {
            }
        }

        public Settings Settings
        {
            get => default;
            set
            {
            }
        }

        //Seleccionar elemento en la lista de navegación
        public void SetSelectedNavigationItem(int index)
        {
            sdPanel.SelectedItem = sdPanel.MenuItems[index];
        }

        //Navegar al elemento correcto de la lista
        private void sdPanel_ItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            FrameNavigationOptions navOptions = new FrameNavigationOptions();
            navOptions.TransitionInfoOverride = args.RecommendedNavigationTransitionInfo;
            Page page = mainPage;
            if (sender.SelectedItem == sender.SettingsItem)
            {
                page = settingsPage;
            }
            else
            {
                page = mainPage;
            }
            contentFrame.Navigate(page.GetType(), navOptions);
            contentFrame.Content = page;
        }
    }
}