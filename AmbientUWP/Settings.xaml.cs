﻿using System;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// La plantilla de elemento Página en blanco está documentada en https://go.microsoft.com/fwlink/?LinkId=234238

namespace AmbientUWP
{
    /// <summary>
    /// Una página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class Settings : Page
    {
        public Settings()
        {
            InitializeComponent();
            if (Data.LocalVar.tree.Count > 0)
                RmvFolder.IsEnabled = true;
            FolderList.ItemsSource = Data.LocalVar.tree;
        }

        //Añadir carpetas de librerias para el árbol
        private async void AddFolder_Click(object sender, RoutedEventArgs e)
        {
            FolderPicker picker = new FolderPicker();
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.MusicLibrary;
            picker.FileTypeFilter.Add("*");
            StorageFolder newFolder = await picker.PickSingleFolderAsync();
            Windows.Storage.AccessCache.StorageApplicationPermissions.MostRecentlyUsedList.Add(newFolder);
            Data.LocalVar.tree.Add(new ExplorerItem(newFolder));
            Data.LocalVar.IndexList();
            if (!RmvFolder.IsEnabled)
                RmvFolder.IsEnabled = true;

        }

        //Elimina una carpeta de librerias del árbol
        private void RmvFolder_Click(object sender, RoutedEventArgs e)
        {
            if (FolderList.Items.Count > 0)
            {
                Data.LocalVar.tree.RemoveAt(FolderList.SelectedIndex);
                Data.LocalVar.IndexList();
            }
            if (FolderList.Items.Count == 0)
                RmvFolder.IsEnabled = false;
        }

        //Limpiar la configuración actual y cargar el fichero
        private void LoadConfigBtn_Click(object sender, RoutedEventArgs e)
        {
            Data.LocalVar.tree.Clear();
            Data.LocalVar.indexedTree.Clear();
            Data.LocalVar.loadSettings();
        }

        //Guardar la config actual en el fichero
        private void SaveConfigBtn_Click(object sender, RoutedEventArgs e)
        {
            Data.LocalVar.saveSettings();
        }

        private void Band1Center_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band1Center.Text, out output))
            {
                Data.Parameters.nonDefaultBands[0, 0] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band2Center_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band2Center.Text, out output))
            {
                Data.Parameters.nonDefaultBands[1, 0] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band3Center_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band3Center.Text, out output))
            {
                Data.Parameters.nonDefaultBands[2, 0] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band4Center_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band4Center.Text, out output))
            {
                Data.Parameters.nonDefaultBands[3, 0] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band5Center_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band5Center.Text, out output))
            {
                Data.Parameters.nonDefaultBands[4, 0] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band6Center_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band6Center.Text, out output))
            {
                Data.Parameters.nonDefaultBands[5, 0] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band7Center_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band7Center.Text, out output))
            {
                Data.Parameters.nonDefaultBands[6, 0] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band8Center_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band8Center.Text, out output))
            {
                Data.Parameters.nonDefaultBands[7, 0] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band9Center_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band9Center.Text, out output))
            {
                Data.Parameters.nonDefaultBands[8, 0] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band10Center_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band10Center.Text, out output))
            {
                Data.Parameters.nonDefaultBands[9, 0] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band1Widht_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band1Widht.Text, out output))
            {
                Data.Parameters.nonDefaultBands[0, 1] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band2Widht_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band2Widht.Text, out output))
            {
                Data.Parameters.nonDefaultBands[1, 1] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band3Widht_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band3Widht.Text, out output))
            {
                Data.Parameters.nonDefaultBands[2, 1] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band4Widht_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band4Widht.Text, out output))
            {
                Data.Parameters.nonDefaultBands[3, 1] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band5Widht_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band5Widht.Text, out output))
            {
                Data.Parameters.nonDefaultBands[4, 1] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band6Widht_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band6Widht.Text, out output))
            {
                Data.Parameters.nonDefaultBands[5, 1] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band7Widht_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band7Widht.Text, out output))
            {
                Data.Parameters.nonDefaultBands[6, 1] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band8Widht_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band8Widht.Text, out output))
            {
                Data.Parameters.nonDefaultBands[7, 1] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band9Widht_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band9Widht.Text, out output))
            {
                Data.Parameters.nonDefaultBands[8, 1] = output;
                Data.Parameters.bandsChanged = true;
            }
        }

        private void Band10Widht_TextChanged(object sender, TextChangedEventArgs e)
        {
            double output;
            if (Double.TryParse(Band10Widht.Text, out output))
            {
                Data.Parameters.nonDefaultBands[9, 1] = output;
                Data.Parameters.bandsChanged = true;
            }
        }
    }
}
